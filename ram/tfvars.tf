variable "workload_account_id" {}
variable "ram_name" {}
variable "share_subnets" {}

variable "tags" {
  type = map(string)
}