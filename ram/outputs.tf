output "aws_ram_resource_id" {
  value = [for value in aws_ram_resource_association.subnet_share : value.id]
}

output "ram_id" {
  value = aws_ram_resource_share.main.id
}