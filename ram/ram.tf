resource "aws_ram_resource_share" "main" {
  name                      = var.ram_name
  allow_external_principals = true
  tags                      = var.tags
}

resource "aws_ram_principal_association" "main" {
  principal          = var.workload_account_id
  resource_share_arn = aws_ram_resource_share.main.arn
}

resource "aws_ram_resource_association" "subnet_share" {
  for_each           = toset(var.share_subnets)
  resource_arn       = each.value
  resource_share_arn = aws_ram_resource_share.main.arn
}