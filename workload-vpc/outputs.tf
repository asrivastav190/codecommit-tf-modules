output "vpc_id" {
  value       = aws_vpc.iso_ne_workload_vpc.id
  description = "The ID of the VPC"
}
output "tgw_subnet_ids" {
  value       = [for subnet in aws_subnet.workload_vpc_private_tgw_subnet : subnet.id]
  description = "Subnet IDs for the /28 subnet for Transit Gateway attachment"
}
output "workload_vpc_rt_id" {
  value       = aws_route_table.workload_vpc_rt.id
  description = "Workload VPC route table identifier"
}
output "workload_vpc_flow_log_ids" {
  value = aws_flow_log.workload_vpc_log[*].id
}
output "subnet_arn" {
  value = [for subnet in aws_subnet.workload_vpc_private_subnet : subnet.arn]
}