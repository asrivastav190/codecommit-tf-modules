
variable "vpc_name" {
  type        = string
  description = "Name for the requested VPC"
}
variable "cidr_ab" {
  type        = string
  description = "CIDR block for requested VPC"
}

variable "flow_log_enable" {}

variable "enable_dns_support" {
  type        = bool
  description = "A boolean flag to enable/disable DNS support in the VPC"
  default     = true
}
variable "enable_dns_hostnames" {
  type        = bool
  description = "A boolean flag to enable/disable DNS hostnames in the VPC"
  default     = true
}

variable "tags" {
  type = map(string)
}

variable "map_public_ip_on_launch" {}

variable "s3_bucket_arn" {
  type        = string
  description = "Flow Log Bucket arn"
}

variable "transit_gateway_id" {
  type        = string
  description = "trsnsit gateway id, used as detination in subnet roye tables"
}

variable "spoke_vpc_inspection_tgw_route_table_id" {
  type        = string
  description = "Spoke/Workload VPC Route Table, used to propogate destination VPC CIDR"
}

variable "firewall_subnet_tgw_route_table_id" {
  type        = string
  description = "Firewall Subnet route table, used to associate with Spoke VPC-TGW attcahment"
}
variable "tgw_subnets" {
  description = "Transit gateway subnet parameters, used to spin off subnets in given workload vpc"
}
variable "workload_subnets" {
  description = "workload subnet parameters, used to spin off private subnets in given workload vpc"
}
