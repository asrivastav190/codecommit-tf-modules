resource "aws_route_table" "workload_vpc_rt" {
  vpc_id = aws_vpc.iso_ne_workload_vpc.id
  route {
    cidr_block         = "0.0.0.0/0"
    transit_gateway_id = var.transit_gateway_id
  }
  tags = merge(var.tags, { "Name" = "${var.vpc_name}-rt-private" })
}

resource "aws_route_table_association" "workload_vpc_rt_assocation" {
  for_each       = var.workload_subnets
  subnet_id      = aws_subnet.workload_vpc_private_subnet[each.key].id
  route_table_id = aws_route_table.workload_vpc_rt.id
}

resource "aws_route_table_association" "workload_vpc_tgw_rt_assocation" {
  for_each       = var.tgw_subnets
  subnet_id      = aws_subnet.workload_vpc_private_tgw_subnet[each.key].id
  route_table_id = aws_route_table.workload_vpc_rt.id
}