# Workload/Spoke VPC
## Description

The requirement is to create a VPC with below components:

-General private workload subnets spread across 4 availability zones having /24 CIDR.  

-Dedicated Transit Gateway attachment subnets with /28 CIDR.  

-All egress traffic must be routed through the Inspection VPC firewall using the Transit Gateway.  

-VPC Flow log’s need to be enabled.  

-A S3 endpoint for each VPC.  

-As per initial deployment, we are creating the two workload VPCs.  
```
    --Development VPC  
    --Production VPC  
```   

## Terraform Module
```
\---workload-vpc
    |   flow_log.tf
    |   s3-endpoint.tf
    |   outputs.tf
    |   providers.tf
    |   README.md
    |   tfvars.tf
    |   subnets.tf
    |   route_table.tf
    |   vpc-tgw-attachment.tf
    |   vpc.tf
```



This workload VPC module will be called from main.tf in the root directory as shown below.

```
module "create_workload_vpc" {
  providers = {
    aws = aws.use1
  }
  source                                  = "bitbucket.org/iso-ne/terraform-modules/workload-vpc"
  for_each                                = var.workload_vpc
  tgw_subnets                             = each.value["tgw_subnets"]
  workload_subnets                        = each.value["workload_subnets"]
  vpc_name                                = each.value["vpc_name"]
  cidr_ab                                 = each.value["cidr_ab"]
  flow_log_enable                         = true
  map_public_ip_on_launch                 = var.map_public_ip_on_launch
  s3_bucket_arn                           = module.create_s3.s3_bucket_arn
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
  tags                                    = var.tags
}
 
## Root Structure
User Inputs: 
The below User Inputs are required and can be customized from the variables.tf file in the root directory.
```
###Workload vpc name, cidr, and flow log enable option. 
variable "workload_vpc" {
  type = map(object({
    vpc_name        = string
    cidr_ab         = string
    flow_log_enable = bool
    tgw_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
    workload_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
  }))
  default = {
    development_vpc = {
      cidr_ab         = "10.165.0.0/16"
      vpc_name        = "development-vpc"
      flow_log_enable = true
      workload_subnets = {
        subnet1 = {
          cidr_block = "10.165.17.0/24"
          name       = "dev-vpc-prv-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.165.18.0/24"
          name       = "dev-vpc-prv-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.165.19.0/24"
          name       = "dev-vpc-prv-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.165.20.0/24"
          name       = "dev-vpc-prv-workload-sn-4"
          az         = "use1-az2"
        }
        #Add subnet block here, in case of another subnet requirement.
      }
      tgw_subnets = {
        subnet1 = {
          cidr_block = "10.165.0.112/28"
          name       = "dev-vpc-prv-tgw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.165.0.128/28"
          name       = "dev-vpc-prv-tgw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.165.0.144/28"
          name       = "dev-vpc-prv-tgw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.165.0.160/28"
          name       = "dev-vpc-prv-tgw-sn-4"
          az         = "use1-az2"
        }
      }
    }
    production_vpc = {
      cidr_ab         = "10.166.0.0/16"
      vpc_name        = "production-vpc"
      flow_log_enable = false
      workload_subnets = {
        subnet1 = {
          cidr_block = "10.166.7.0/24"
          name       = "prod-vpc-prv-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.166.8.0/24"
          name       = "prod-vpc-prv-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.166.9.0/24"
          name       = "prod-vpc-prv-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.166.10.0/24"
          name       = "prod-vpc-prv-workload-sn-4"
          az         = "use1-az2"
        }
      }
      tgw_subnets = {
        subnet1 = {
          cidr_block = "10.166.0.112/28"
          name       = "prod-vpc-prv-tgw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.166.0.128/28"
          name       = "prod-vpc-prv-tgw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.166.0.144/28"
          name       = "prod-vpc-prv-tgw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.166.0.160/28"
          name       = "prod-vpc-prv-tgw-sn-4"
          az         = "use1-az2"
        }
      }
    }
    #Add another vpc block here in case of new Private Workload vpc requirement.
  }
}
``` 

VPC Name and VPC CIDR: Provide a VPC name and CIDR mapping in the variable workload_vpc. As part of initial deployment, four two VPCs are being created. If any new VPCs are required the same can be added in the variable map.

***Remember to update this variable as per mapping in the account***
![image1](../images/workload-vpc/az_mapping.PNG)

## Automated Inputs: 
Below are inputs passed into the workload_vpc_tgw_attachment module from the outputs of transit gateway module.

spoke_vpc_inspection_tgw_route_table_id: This Transit Gateway route table is associated with the Transit Gateway and spoke VPC attachment. The attachment creation can be found in vpc-tgw-attachement.tf in the workload-vpc module.

firewall_subnet_tgw_route_table_id: This route table is used to propagate the spoke VPC CIDR route to its Transit Gateway route table. 
transit_gateway_id: The ID for the Transit Gateway in the us-east-1 region is passed to attach the Transit Gateway with the created spoke VPC.
```
## Core Module: The core workload/spoke VPC module is structured as shown below.
```
\---workload-vpc
    |   data.tf
    |   flow_log.tf
    |   vpc-tgw-attachment.tf
    |   outputs.tf
    |   providers.tf
    |   README.md
    |   tfvars.tf
    |   subnets.tf
    |   route-table.tf
    |   vpc.tf
    |   s3-endpoint.tf
```
Resources
VPC: A VPC with the passed CIDR range and Name, s3 endpoint and flow log enabled.
Subnets: Two private subnets in each AZ (four AZs) will be created. Transit Gateway subnets will be a /28 CIDR and other subnets will be a /24 CIDR.
Route Table: The route table will be created for each workload VPC and will be associated with all subnets in that VPC. This route table will have one default route to the Transit Gateway.
S3 Endpoint : One S3 endpoint will,be created along with VPC and all private subnets in vpc will have internal S3 connectivity.
VPC Flow Log : Based on the flow_log_enable option in varibales.tf, vpc flow logs will be enabled for vpc having s3 as destination log bucket.

```
Destination   Target
0.0.0.0/0   Transit Gateway ID
```

VPC-Transit Gateway attachment: The newly created spoke/workload VPC will be attached to the transit gateway and a VPC CIDR route will be propagated to the spoke_vpc_inspection_tgw_route_table.
The firewall_subnet_tgw_route_table will be associated with this VPC -Transit Gateway attachment.
#### Diagram
This module implenments the below VPC architecture.

![image2](../images/workload-vpc/workloadvpcarch.PNG)

#### Reference Links
N/A

#### Limitations
N/A
