## Workload VPC private subnets
resource "aws_subnet" "workload_vpc_private_subnet" {
  for_each                = var.workload_subnets
  vpc_id                  = aws_vpc.iso_ne_workload_vpc.id
  availability_zone_id    = each.value.az
  cidr_block              = each.value.cidr_block
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", each.value.name)
    }
  )
}

## Workload vpc transit gateway subnets
resource "aws_subnet" "workload_vpc_private_tgw_subnet" {
  for_each                = var.tgw_subnets
  vpc_id                  = aws_vpc.iso_ne_workload_vpc.id
  availability_zone_id    = each.value.az
  cidr_block              = each.value.cidr_block
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", each.value.name)
    }
  )
}
