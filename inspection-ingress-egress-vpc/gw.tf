# /*
# This terraform template will create the following resources,
# 1. Internet gateway
# 2. NAT gateway
# */

# # create Ingress Internet gateway 
resource "aws_internet_gateway" "ingress_igw" {
  vpc_id = aws_vpc.ingress_vpc.id
  tags   = merge(var.tags, { "Name" = format("%s-igw", var.ingress_vpc_param.vpc_name) })
}

# # create Egress Internet gateway 
resource "aws_internet_gateway" "egress_igw" {
  vpc_id = aws_vpc.egress_vpc.id
  tags   = merge(var.tags, { "Name" = format("%s-igw", var.egress_vpc_param.vpc_name) })
}

# # create Egress VPC EIP for each nat gateway

resource "aws_eip" "nat_eip" {
  for_each = var.az_zone
  vpc      = true
  tags = merge(
    var.tags,
    tomap({
      "Name" = format("%s-eip-%s", var.egress_vpc_param.vpc_name, lower(each.value))
    })
  )
}


resource "aws_nat_gateway" "nat" {
  for_each      = var.az_zone
  allocation_id = aws_eip.nat_eip[each.value].id
  subnet_id     = aws_subnet.pub_egress[each.value].id

  tags = merge(
    var.tags,
    tomap({
      "Name" = format("%s-nat-%s", var.egress_vpc_param.vpc_name, lower(each.value))
    })
  )
}
