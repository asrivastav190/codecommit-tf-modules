resource "aws_flow_log" "inspection_vpc_log" {
  count                = var.inspection_vpc_param.flow_log_enable ? 1 : 0
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = aws_vpc.inspection_vpc.id
  tags                 = merge(var.tags, { "Name" = format("%s-flowlog", var.inspection_vpc_param.vpc_name) })
  depends_on           = [aws_vpc_endpoint.s3_inspection_vpc, aws_vpc_endpoint_route_table_association.private_s3_inspection_tgw_rt, aws_vpc_endpoint_route_table_association.private_s3_inspection_nfw_rt]
}

resource "aws_flow_log" "ingress_vpc_log" {
  count                = var.ingress_vpc_param.flow_log_enable ? 1 : 0
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = aws_vpc.ingress_vpc.id
  tags                 = merge(var.tags, { "Name" = format("%s-flowlog", var.ingress_vpc_param.vpc_name) })
  depends_on           = [aws_vpc_endpoint.s3_ingress_vpc, aws_vpc_endpoint_route_table_association.private_s3_ingress_tgw_rt]
}

resource "aws_flow_log" "egress_vpc_log" {
  count                = var.egress_vpc_param.flow_log_enable ? 1 : 0
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = aws_vpc.egress_vpc.id
  tags                 = merge(var.tags, { "Name" = format("%s-flowlog", var.egress_vpc_param.vpc_name)})
  depends_on           = [aws_vpc_endpoint.s3_egress_vpc, aws_vpc_endpoint_route_table_association.private_s3_egress_tgw_rt]
}