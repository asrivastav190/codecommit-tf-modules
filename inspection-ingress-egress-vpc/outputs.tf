## Inspection VPC & Subnets
output "inspection_vpc_id" {
  value = aws_vpc.inspection_vpc.id
}

output "prv_tgw_subnet_ids" {
  value = [for value in aws_subnet.prv_tgw_inspection : value.id]
}

output "prv_nfw_subnet_ids" {
  value = [for value in aws_subnet.prv_nfw_inspection : value.id]
}

output "inspection_tgw_rt_id" {
  value = aws_route_table.prv_tgw_inspection.id
}

output "inspection_nfw_rt_id" {
  value = aws_route_table.prv_nfw_inspection.id
}

## Ingress VPC & Subnets
output "ingress_vpc_id" {
  value = aws_vpc.ingress_vpc.id
}

output "ingress_pub_subnet_ids" {
  value = [for value in aws_subnet.pub_ingress : value.id]
}

output "ingress_prv_subnet_ids" {
  value = [for value in aws_subnet.prv_tgw_ingress : value.id]
}

output "ingress_pub_rt_id" {
  value = aws_route_table.pub_ingress.id
}

output "ingress_prv_rt_id" {
  value = aws_route_table.prv_tgw_ingress.id
}

## Egress VPC , Subnets and Route Tables
output "egress_vpc_id" {
  value = aws_vpc.egress_vpc.id
}

output "egress_pub_subnet_ids" {
  value = [for value in aws_subnet.pub_egress : value.id]
}

output "egress_prv_subnet_ids" {
  value = [for value in aws_subnet.prv_tgw_egress : value.id]
}

output "egress_pub_rt_id" {
  value = aws_route_table.pub_egress.id
}

output "egress_prv_rt_id" {
  value = aws_route_table.prv_tgw_egress.id
}

##VPC Attachment output

output "inspection_vpc_tgw_attachment_id" {
  value       = aws_ec2_transit_gateway_vpc_attachment.inspection_vpc_tgw_attachment.id
  description = "Transit gateway VPC identifier for Inspection VPC attachment"
}

output "ingress_vpc_tgw_attachment_id" {
  value       = aws_ec2_transit_gateway_vpc_attachment.ingress_vpc_tgw_attachment.id
  description = "Transit gateway VPC identifier for Ingress VPC attachment"
}

output "central_egress_vpc_tgw_attachment_id" {
  value       = aws_ec2_transit_gateway_vpc_attachment.egress_vpc_tgw_attachment.id
  description = "Transit gateway VPC identifier for Egress VPC attachment"
}