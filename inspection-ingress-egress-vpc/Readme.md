# AWS Inspection VPC

#### Description

>The Inspection VPC consists of two subnets in each AZ. One subnet is a dedicated firewall endpoint subnet and the second is dedicated to an AWS Transit Gateway attachment.

>Each Transit Gateway subnet requires a dedicated VPC route table to ensure the traffic is forwarded to the firewall endpoint.

>These route tables have a default route (0.0.0.0/0) pointing towards the firewall endpoint in the same AZ.

> For the return traffic from the firewall endpoint, a single VPC route table is configured. The route table contains a default route towards AWS Transit Gateway. 

>Traffic is returned to the AWS Transit Gateway in the same AZ after it has been inspected by AWS Network Firewall.

## Terraform Module
```
+---modules
|   +---inspection-ingress-egress-vpc
|   |   |   gw.tf
|   |   |   flowlog.tf
|   |   |   outputs.tf
|   |   |   providers.tf
|   |   |   tfvars.tf
|   |   |   subnets.tf
|   |   |   vpc.tf
|   |   |   README.md
|   |   |   s3-endpoint.tf
|   |   |   route-table.tf
|   |   |   vpc-tgw-attachement.tf
```


Inspection VPC module contains below files and folders:

**Files:**
Flowlog.tf : To enable VPC flow logs for inspection VPC.

subnets.tf : This file is responsible for creating transit gateway subnets for inspection vpc.

vpc.tf: Terraform file used to create the Inspection VPC.

gw.tf: Not applicable in the case of Inspection VPC.
  
outputs.tf: This is responsible for logging the output of the Inspection VPC ID,Ingress VPC ID,Egress VPC ID , VPC flow log IDs for each VPC and private transit subnet ids.

tfvars.tf: This Terraform file is used to pass input variables like the Inspection VPC CIDR range, tags, environment, availability zone, Transit Gateway ID, its route table and NACL rules. All these parameters are passed by the root folder variables.tf file.

#### Reference Links
https://aws.amazon.com/blogs/networking-and-content-delivery/deployment-models-for-aws-network-firewall/

#### Diagram
 ![Image](../images/vpc/inspection-ingress-egress/arch_snap.png)

#### Limitations
N/A

________________________________________
# AWS Ingress VPC

#### Description
●	The Ingress VPC consists of two subnets in each AZ. One subnet is dedicated to the AWS Transit Gateway attachment and the second is a public subnet.
●	Each Transit Gateway subnet requires a dedicated VPC route table to ensure the traffic is forwarded to the Transit Gateway via internet gateway in the public subnet. 
●	Traffic is then forwarded to the Ingress VPC.
●	These route tables have route (10.0.0.0/8) pointing towards the Transit Gateway in the same AZ.

Reference Links
https://aws.amazon.com/blogs/networking-and-content-delivery/deployment-models-for-aws-network-firewall/

#### Diagram
 ![Image](../images/vpc/inspection-ingress-egress/arch_snap.png)

#### Limitations
N/A
________________________________________
# AWS Egress VPC

#### Description
●	The Egress VPC consists of two subnets in each AZ. One subnet is dedicated to the AWS Transit Gateway attachment and the second is the public subnet.
●	Each Transit Gateway subnet requires a dedicated VPC route table attached to the NAT and Transit Gateway whereas the public subnet is attached to the Transit Gateway and the internet gateway.
●	These route tables have route (10.0.0.0/8) pointing towards the Transit Gateway in the same AZ.
●	The configuration centralizes outbound internet traffic from many VPCs without compromising VPC isolation.

#### Reference Links
https://aws.amazon.com/blogs/networking-and-content-delivery/deployment-models-for-aws-network-firewall/


#### Diagram
 
![Image](../images/vpc/inspection-ingress-egress/arch_snap.png)

#### Limitations
N/A