# # # /*
# # # This terraform template will create the following resources,
# # # 1. One Public Router table for Public Subnet 1A 1B & 1C
# # # 2. Three Private Route table for app,db and analytics private subnets
# # # */

# # #Public Ingress Route Table
resource "aws_route_table" "pub_ingress" {
  vpc_id = aws_vpc.ingress_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ingress_igw.id
  }
  route {
    cidr_block         = "10.0.0.0/8"
    transit_gateway_id = var.transit_gateway_id
  }
  tags = merge(var.tags, { "Name" = format("%s-%s", var.ingress_vpc_param.vpc_name,"pub-sn-rt") })
}

resource "aws_route_table_association" "pub_ingress" {
  for_each       = var.az_zone
  subnet_id      = aws_subnet.pub_ingress[each.key].id
  route_table_id = aws_route_table.pub_ingress.id
}
# # ## Private Ingress TGW RT
resource "aws_route_table" "prv_tgw_ingress" {
  vpc_id = aws_vpc.ingress_vpc.id

  route {
    cidr_block         = "10.0.0.0/8"
    transit_gateway_id = var.transit_gateway_id
  }
  tags = merge(var.tags, { "Name" = format("%s-%s", var.ingress_vpc_param.vpc_name,"tgw-sn-rt")})
}
resource "aws_route_table_association" "prv_tgw_ingress" {
  for_each       = var.az_zone
  subnet_id      = aws_subnet.prv_tgw_ingress[each.value].id
  route_table_id = aws_route_table.prv_tgw_ingress.id
}

# # #Public Egress Route Table
resource "aws_route_table" "pub_egress" {
  vpc_id = aws_vpc.egress_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.egress_igw.id
  }

  route {
    cidr_block         = "10.0.0.0/8"
    transit_gateway_id = var.transit_gateway_id
  }
  tags =  merge(var.tags, { "Name" = format("%s-%s", var.egress_vpc_param.vpc_name,"pub-sn-rt")})
}

resource "aws_route_table_association" "pub_egress" {
  for_each       = var.az_zone
  subnet_id      = aws_subnet.pub_egress[each.value].id
  route_table_id = aws_route_table.pub_egress.id
}

# # ## Private Egress TGW RT
resource "aws_route_table" "prv_tgw_egress" {
  vpc_id = aws_vpc.egress_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = element([for value in aws_nat_gateway.nat : value.id], 0)
  }

  route {
    cidr_block         = "10.0.0.0/8"
    transit_gateway_id = var.transit_gateway_id
  }
  tags = merge(var.tags, { "Name" = format("%s-%s", var.egress_vpc_param.vpc_name,"tgw-sn-rt")})
}

resource "aws_route_table_association" "prv_tgw_egress" {
  for_each       = var.az_zone
  subnet_id      = aws_subnet.prv_tgw_egress[each.value].id
  route_table_id = aws_route_table.prv_tgw_egress.id
}

# # ## Private TGW Inspection RT

resource "aws_route_table" "prv_tgw_inspection" {
  vpc_id = aws_vpc.inspection_vpc.id
  tags   = merge(var.tags, { "Name" = format("%s-%s", var.inspection_vpc_param.vpc_name,"tgw-sn-rt")})
}

resource "aws_route_table_association" "prv_tgw_inspection" {
  for_each       = var.az_zone
  subnet_id      = aws_subnet.prv_tgw_inspection[each.value].id
  route_table_id = aws_route_table.prv_tgw_inspection.id
}

# # ## Private NFW Inspection RT

resource "aws_route_table" "prv_nfw_inspection" {
  vpc_id = aws_vpc.inspection_vpc.id
  route {
    cidr_block         = "0.0.0.0/0"
    transit_gateway_id = var.transit_gateway_id
  }
  tags =  merge(var.tags, { "Name" = format("%s-%s", var.inspection_vpc_param.vpc_name,"nfw-sn-rt")})
}

resource "aws_route_table_association" "prv_nfw_inspection" {
  for_each       = var.az_zone
  subnet_id      = aws_subnet.prv_nfw_inspection[each.value].id
  route_table_id = aws_route_table.prv_nfw_inspection.id
}