resource "aws_vpc" "inspection_vpc" {
  cidr_block           = var.inspection_vpc_param.cidr_ab
  enable_dns_hostnames = true
  tags                 = merge(var.tags, { "Name" = var.inspection_vpc_param.vpc_name })
}

resource "aws_vpc" "egress_vpc" {
  cidr_block           = var.egress_vpc_param.cidr_ab
  enable_dns_hostnames = true
  tags                 = merge(var.tags, { "Name" = var.egress_vpc_param.vpc_name })
}

resource "aws_vpc" "ingress_vpc" {
  cidr_block           = var.ingress_vpc_param.cidr_ab
  enable_dns_hostnames = true
  tags                 = merge(var.tags, { "Name" = var.ingress_vpc_param.vpc_name })
}