variable "inspection_vpc_param" {
  description = "Inspection VPC Parameters"
}

variable "ingress_vpc_param" {
  description = "Ingress VPC Parameters"
}

variable "egress_vpc_param" {
  description = "Egress VPC Parameters"
}

variable "s3_bucket_arn" {
  description = "ARN for the flowlog bucket"
}

variable "global" {
  description = "Global map of variables"
  type        = map(string)
  default = {
    "inspection_env" = "InspectionVPC",
    "ingress_env"    = "IngressVPC",
    "egress_env"     = "EgressVPC",
    "region"         = "us-east-1"
  }
}

variable "az_zone" {
  description = "List of zones to be initialized (subnets, gw etc) in vpc"
  type        = set(string)
}

variable "az_nums" {
  type = map(string)
}

variable "tags" {
  type = map(string)
}

variable "transit_gateway_id" {
  type        = string
  description = "Identifier for the Transit Gateway that will be associated with this VPC"
}
variable "region" {
 type = string
 description = "AWS region to deploy resources"  
}

### Transit Gateway Route Table Identifier for Transit Gateway VPC attachment
variable "spoke_vpc_inspection_tgw_route_table_id" {
  type        = string
  description = "Transit Gateway Route Table Identifier for Spoke VPC Inpection TGW Route Table"
}

variable "firewall_subnet_tgw_route_table_id" {
  type        = string
  description = "Transit Gateway Route Table Identifier for Firewall Sunbnet TGW Route Table"
}