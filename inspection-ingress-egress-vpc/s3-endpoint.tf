######################
# VPC Endpoint for S3
######################

data "aws_vpc_endpoint_service" "s3" {
  service      = "s3"
  service_type = "Gateway"
}

### S3 Endpoint for ingress VPC
resource "aws_vpc_endpoint" "s3_ingress_vpc" {
  vpc_id       = aws_vpc.ingress_vpc.id
  service_name = data.aws_vpc_endpoint_service.s3.service_name
  tags         = merge(var.tags, { "Name" = format("%s-%s", var.ingress_vpc_param.vpc_name, "s3-endpoint")})
}

resource "aws_vpc_endpoint_route_table_association" "private_s3_ingress_tgw_rt" {
  vpc_endpoint_id = aws_vpc_endpoint.s3_ingress_vpc.id
  route_table_id  = aws_route_table.prv_tgw_ingress.id
}

### S3 Endpoint for Egress VPC

resource "aws_vpc_endpoint" "s3_egress_vpc" {
  vpc_id       = aws_vpc.egress_vpc.id
  service_name = data.aws_vpc_endpoint_service.s3.service_name
  tags         = merge(var.tags, { "Name" = format("%s-%s", var.egress_vpc_param.vpc_name, "s3-endpoint")})
}
resource "aws_vpc_endpoint_route_table_association" "private_s3_egress_tgw_rt" {
  vpc_endpoint_id = aws_vpc_endpoint.s3_egress_vpc.id
  route_table_id  = aws_route_table.prv_tgw_egress.id
}


### S3 Endpoint for Inspection VPC
resource "aws_vpc_endpoint" "s3_inspection_vpc" {
  vpc_id       = aws_vpc.inspection_vpc.id
  service_name = data.aws_vpc_endpoint_service.s3.service_name
  tags         = merge(var.tags, { "Name" = format("%s-%s", var.inspection_vpc_param.vpc_name, "s3-endpoint")})
}

resource "aws_vpc_endpoint_route_table_association" "private_s3_inspection_tgw_rt" {
  vpc_endpoint_id = aws_vpc_endpoint.s3_inspection_vpc.id
  route_table_id  = aws_route_table.prv_tgw_inspection.id
}

resource "aws_vpc_endpoint_route_table_association" "private_s3_inspection_nfw_rt" {
  vpc_endpoint_id = aws_vpc_endpoint.s3_inspection_vpc.id
  route_table_id  = aws_route_table.prv_nfw_inspection.id
}