# # /*
# # this terraform template will create public and private subnets.
# # */

# # # public ingress subnets
resource "aws_subnet" "pub_ingress" {
  for_each          = var.az_zone
  vpc_id            = aws_vpc.ingress_vpc.id
  cidr_block        = cidrsubnet(var.ingress_vpc_param.cidr_ab, 8, 0 + var.az_nums[each.value])
  availability_zone = "${var.region}${each.value}"
  tags              = merge(var.tags, { "Name" = format("%s-%s-%s", var.ingress_vpc_param.vpc_name,"pub-sn",lower(each.value)) })
}

# # # private ingress tgw subnets
resource "aws_subnet" "prv_tgw_ingress" {
  for_each          = var.az_zone
  vpc_id            = aws_vpc.ingress_vpc.id
  cidr_block        = cidrsubnet(var.ingress_vpc_param.cidr_ab, 12, 0 + var.az_nums[each.value])
  availability_zone = "${var.region}${each.value}"
  tags              = merge(var.tags, { "Name" = format("%s-%s-%s", var.ingress_vpc_param.vpc_name, "tgw-sn", lower(each.value)) })
}

# # # public egress subnets
resource "aws_subnet" "pub_egress" {
  for_each          = var.az_zone
  vpc_id            = aws_vpc.egress_vpc.id
  cidr_block        = cidrsubnet(var.egress_vpc_param.cidr_ab, 8, 0 + var.az_nums[each.value])
  availability_zone = "${var.region}${each.value}"
  tags              = merge(var.tags, { "Name" = format("%s-%s-%s", var.egress_vpc_param.vpc_name,"pub-sn", lower(each.value)) })
}

# # # private egress tgw subnets
resource "aws_subnet" "prv_tgw_egress" {
  for_each          = var.az_zone
  vpc_id            = aws_vpc.egress_vpc.id
  cidr_block        = cidrsubnet(var.egress_vpc_param.cidr_ab, 12, 0 + var.az_nums[each.value])
  availability_zone = "${var.region}${each.value}"
  tags              = merge(var.tags, { "Name" = format("%s-%s-%s", var.egress_vpc_param.vpc_name,"tgw-sn", lower(each.value)) })
}

# # # # # #Private TGW Subnets
resource "aws_subnet" "prv_tgw_inspection" {
  for_each = var.az_zone

  vpc_id            = aws_vpc.inspection_vpc.id
  cidr_block        = cidrsubnet(var.inspection_vpc_param.cidr_ab, 12, 0 + var.az_nums[each.value])
  availability_zone = "${var.region}${each.value}"
  tags              = merge(var.tags, { "Name" = format("%s-%s-%s", var.inspection_vpc_param.vpc_name,"tgw-sn", lower(each.value)) })
}

# # # # # #Private NFW Subnets
resource "aws_subnet" "prv_nfw_inspection" {
  for_each = var.az_zone

  vpc_id            = aws_vpc.inspection_vpc.id
  cidr_block        = cidrsubnet(var.inspection_vpc_param.cidr_ab, 12, 20 + var.az_nums[each.value])
  availability_zone = "${var.region}${each.value}"
  tags              = merge(var.tags, { "Name" = format("%s-%s-%s", var.inspection_vpc_param.vpc_name, "nfw-sn", lower(each.value)) })
}