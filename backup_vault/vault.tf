# AWS Backup vault
resource "aws_backup_vault" "ab_vault" {
  count = var.enable_vault && var.vault_name != null ? 1 : 0
  name  = var.vault_name
  tags  = local.common_tags
}

data "aws_caller_identity" "current" {}

locals {
  # Common tags to be assigned to all resources
  common_tags = {
    "account_id"              = data.aws_caller_identity.current.account_id,
    "iso-environment"         = "sandbox",
    "iso-contact"             = "eg@iso-ne.com",
    "iso-deployment-artifact" = "cmdb"

  }
}
