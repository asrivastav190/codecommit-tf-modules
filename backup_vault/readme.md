## Backup Vault
## Service: AWS Backup Vault
## Description

##### Note : backup vault code is added in aft global customization #####

In AWS Backup, a backup vault is a container that stores and organizes your backups.When creating a backup vault, you must specify the AWS Key Management Service (AWS KMS) encryption key that encrypts some of the backups placed in this vault. Encryption for other backups is managed by their source AWS services.
 
##### Terraform Module

```
+---backup_vault
|       output.tf
|       providers.tf
|       readme.md
|       tfvars.tf
|       vault.tf
```


Vault.tf: This file is responsible for creating a backup vault.


![Image](../images/backup_vault/vault_tf.PNG)



Output.tf: Display the Vault ID and ARN.

![Image](../images/backup_vault/output_tf.PNG)

tfvars.tf: This file is responsible for passing the backup vault name and enabling/disabling the state of the vault.


![Image](../images/backup_vault/tfvars_tf.PNG)

##### Root Structure
```
|   main.tf
|   outputs.tf
|   providers.tf
|   variables.tf
```
Calling module of backup vault.


![Image](../images/backup_vault/main_tf.PNG)


##### Reference Links

 https://docs.aws.amazon.com/aws-backup/latest/devguide/creating-a-vault.html
 https://docs.aws.amazon.com/aws-backup/latest/devguide/vaults.html

##### Diagram

![Image](../images/backup_vault/arch.png)

##### Limitations
N/A

