# Vault
output "vault_id" {
  description = "The name of the vault"
  value       = join("", aws_backup_vault.ab_vault.*.id)
}

output "vault_arn" {
  description = "The ARN of the vault"
  value       = join("", aws_backup_vault.ab_vault.*.arn)
}
