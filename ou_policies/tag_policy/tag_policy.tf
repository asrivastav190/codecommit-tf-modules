# #### TAG_POLICY
resource "aws_organizations_policy" "tag_policy" {
  name = var.tag_policy_name
  type = "TAG_POLICY"
  description = "Tag policy"
  tags   = var.tags
  content = <<CONTENT
{
  "tags": {
      "Name": {
      "tag_key": {
        "@@assign": "Name"
      },
      "tag_value": {
        "@@assign": [
          "${var.name}"
        ]
      }
    },
    "iso-environment": {
      "tag_key": {
        "@@assign": "iso-environment"
      },
      "tag_value": {
        "@@assign": [
           "${var.iso_environment_value}"
        ]
      }
    },
	"iso-deployment-artifact": {
      "tag_key": {
        "@@assign": "iso-deployment-artifact"
      },
      "tag_value": {
        "@@assign": [
          "${var.iso_deployment_artifact_value}"
        ]
      }
    },
    "iso-activity-code": {
      "tag_key": {
        "@@assign": "iso-activity-code"
      },
      "tag_value": {
        "@@assign": [
          "${var.iso_activity_code}"
        ]
      }
    },
    "iso-contact": {
      "tag_key": {
        "@@assign": "iso-contact"
      },
      "tag_value": {
        "@@assign": [
          "${var.iso_contact_value}"
        ]
      }
    }
  }
}
CONTENT
}

resource "aws_organizations_policy_attachment" "tag_policy" {
  policy_id = aws_organizations_policy.tag_policy.id
  target_id = data.aws_organizations_organizational_units.ou.id
}