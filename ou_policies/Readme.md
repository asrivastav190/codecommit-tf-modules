# SCP & Tag Policy
#### Description
A service control policy implemented using Terraform to deny marketplace access and a tag policy to standardize tags across resources.


#### Terraform Module
```
+--- service_control_policy
|   |       data.tf
|   |       deny_marketplace_access.tf
|   |       outputs.tf
|   |       providers.tf
|   |       service_control_policies.tf
|   |       sso.tf
|   |       tfvars.tf
```

ou_policies module contains below two folders:
##### 1.)	Service_control_policy

data.tf: This file is responsible for fetching the organization unit ID to the policy that will be associated.

deny_marketplace_access.tf: This file is used to create a policy to block access to the marketplace.

outputs.tf: Used to define output values like organization unit ID, policy ID, and account ID where the SCP is being created and the SSO role ARN.

providers.tf: Used to define the management account profile. [Keys were exported as environmental variables]

service_control_policies.tf: Used to create the service control policy to deny marketplace access and associate it to the organization unit; in this case sandbox.

sso.tf: This file is responsible for creating the SSO role along with the appropriate policy attached to the group (“Full-Access”), which is taken as user input.

tfvars.tf: Used to define variables like sandbox account ID to which the SSO role will be attached, marketplace rule name, group name, and SSO permission set name.

##### 2.)	Tag_policy

data.tf: This file is responsible for fetching the organization unit ID to the policy that will be associated.

Outputs.tf: Used to define output values like organization unit ID, policy ID, and account ID where the SCP is being created and a SSO role ARN.

providers.tf: Used to define the management account profile.  **[Keys were exported as environmental variables]**

tag_policy.tf: Used to create a tag policy to ensure standard tags across resources like name, **ISO environment, deployment artifact and contact details.**

tfvars.tf: Used to define variables like tags, tag policy name, and tag parameters to be attached to all resources.

#### Root Structure
```
|   main.tf
|   outputs.tf
|   providers.tf
|   variables.tf
```

main.tf: Below is the reference screenshot of how the ou_policies module is being called by SCP and tag_policy to call each folder inside the module.

 

**NOTE: Regex has been used to validate each tag parameters like name, iso_environment, iso_deployment_artifact and iso_contact in the variable file.**

Variable.tf

Below is the reference screenshot of variables being passed to the service control policy folder of the ou_policies module:
###### group_name: An identifier for an object in SSO, such as a user or group.
###### sso_perms_name: Name of SSO permission.
###### marketplace_rule_name: Name of deny marketplace rule.
 ![Image](../images/ou_policies/scp/scp_variables.png)

Below are the reference screenshots of tag policy variables:
###### tag_policy_name: Name of OU tag policy.
**Following are the mandate tags:**
###### Name: Tag name should contain lower case , numeric value and hyphen. For eg: abc-123
###### Iso_deployment_artifact_value: This particular tag should contain “cmdb-” followed by any serial number.
###### Iso_environment_value: Following are the options: development,integration,pre-production,production,sandbox and user. Make sure appropriate option is selected.
###### Iso_contact_value: This tag specifies the email id and should be in format alpha-num@iso-ne.com.
 ![Image](../images/ou_policies/scp/tag_variables_1.png)
 ![Image](../images/ou_policies/scp/tag_variables_2.png)

##### Reference Links
> How SCP’s work: https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_scps.html

> How Tagging works via SCP: https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_tag-policies.html

##### Diagram
N/A
##### Limitations
N/A


---
___

## Organization Backup Policy

## Description
This policy specifies the backup plan settings that apply to an AWS account. It’s the aggregation of all backup policies that the account inherits, plus any backup policy that is directly attached to the account. When you attach a backup policy to the organization's root, it applies to all accounts in your organization. When you attach a backup policy to an organizational unit (OU), it applies to all accounts and OUs that belong to the OU.

## Reference Links
https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_backup_effective.html

#### Terraform Module
```
+---ou_policies
|   |   Readme.md
|   |
|   +---backup_policy
|   |       backup_policy.tf
|   |       data.tf
|   |       outputs.tf
|   |       providers.tf
|   |       tfvars.tf
```

data.tf: This file is responsible for fetching the organization unit ID to the policy that will be associated with it.


![Image](../images/ou_policies/backup_policy/data_tf.PNG)

outputs.tf: Used to define output values like organization unit ID, policy ID, and account ID where the SCP is being created and a SSO role ARN.


![Image](../images/ou_policies/backup_policy/output_tf.PNG)

providers.tf: Used to define the management account profile. [Keys were exported as environmental variables]

backup_policy.tf: Responsible for scheduling backups of all specified AWS resources containing the tag_key :  () and tag_value () as per the requirements.


![Image](../images/ou_policies/backup_policy/backup_policy_tf.PNG)

tfvars.tf :


![Image](../images/ou_policies/backup_policy/tfvars_tf.PNG)

#### Root Structure
```
|   main.tf
|   outputs.tf
|   providers.tf
|   variables.tf

Backup-policy.json:
```


![Image](../images/ou_policies/backup_policy/backup_policy_json.PNG)

Note: We can use the same backup policy for both the production and non-production OU and specify the below mentioned parameters as per the requirements from variable.tf of root module


Schedule Expression: Specifies the start time of the backup.

target_backup_vault_name: This policy key maps to the TargetBackupVaultName key in an AWS Backup plan.

start_backup_window_minutes: Specifies the number of minutes to wait before         canceling a job that does not start successfully.

complete_backup_window_minutes: Specifies the number of minutes after a backup job successfully starts before it must complete or it is canceled by AWS Backup. 

move_to_cold_storage_after_days: Specifies the number of days after the backup occurs before AWS Backup moves the recovery point to cold storage. 

delete _after_days: Specifies the number of days after the backup occurs before AWS Backup deletes the recovery point. 

 tag_key: Specifies the tag key name to attach to the backup plan.

 tag_value:Specifies the value that is attached to the backup plan and associated with the tag_key.

advance_backup_settings: Specifies settings for specific backup scenarios.

#### Reference Links 
https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_backup_syntax.html?icmpid=docs_orgs_console

https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_backup_effective.html

		
Note: You will call backup_vault in each AWS account to store the AWS resource backup of respective accounts. The backup_vault module is responsible for creating the actual backup vault in each account.
(As a part AFT global customization back vault will create in at the time of account creation)

main.tf: Below is the reference screenshot of how the ou_policies module is being called by backup_policy to call each folder inside the module.


![Image](../images/ou_policies/backup_policy/main_tf.PNG)

variable.tf


![Image](../images/ou_policies/backup_policy/variable_tf.PNG)

Diagram


![Image](../images/ou_policies/backup_policy/backup_design.PNG)

##### Limitations
N/A
 
