data "aws_ssoadmin_instances" "example" {}

data "aws_identitystore_group" "example" {
  identity_store_id = tolist(data.aws_ssoadmin_instances.example.identity_store_ids)[0]

  filter {
    attribute_path  = "DisplayName"
    attribute_value = var.group_name
  }
}

resource "aws_ssoadmin_account_assignment" "example" {
  instance_arn       = aws_ssoadmin_permission_set.allowed_marketplace.instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.allowed_marketplace.arn

  principal_id   = data.aws_identitystore_group.example.group_id
  principal_type = "GROUP"

  target_id   = var.sandbox_account_id
  target_type = "AWS_ACCOUNT"
}
resource "aws_ssoadmin_permission_set" "allowed_marketplace" {
  name         = var.sso_perms_name
  instance_arn = tolist(data.aws_ssoadmin_instances.example.arns)[0]
}

data "aws_iam_policy_document" "awsmarketplacefullaccess" {
  statement {
    effect = "Allow"
    actions = [
                "aws-marketplace:*",
                "cloudformation:CreateStack",
                "cloudformation:DescribeStackResource",
                "cloudformation:DescribeStackResources",
                "cloudformation:DescribeStacks",
                "cloudformation:List*",
                "ec2:AuthorizeSecurityGroupEgress",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:CreateSecurityGroup",
                "ec2:CreateTags",
                "ec2:DescribeAccountAttributes",
                "ec2:DescribeAddresses",
                "ec2:DeleteSecurityGroup",
                "ec2:DescribeAccountAttributes",
                "ec2:DescribeImages",
                "ec2:DescribeInstances",
                "ec2:DescribeKeyPairs",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeSubnets",
                "ec2:DescribeTags",
                "ec2:DescribeVpcs",
                "ec2:RunInstances",
                "ec2:StartInstances",
                "ec2:StopInstances",
                "ec2:TerminateInstances"
            ]
    resources = [ 
        "*",
        ]
  }
    statement {
     effect = "Allow"
     actions = [
                "ec2:CopyImage",
                "ec2:DeregisterImage",
                "ec2:DescribeSnapshots",
                "ec2:DeleteSnapshot",
                "ec2:CreateImage",
                "ec2:DescribeInstanceStatus",
                "ssm:GetAutomationExecution",
                "ssm:UpdateDocumentDefaultVersion",
                "ssm:CreateDocument",
                "ssm:StartAutomationExecution",
                "ssm:ListDocuments",
                "ssm:UpdateDocument",
                "ssm:DescribeDocument",
                "sns:ListTopics",
                "sns:GetTopicAttributes",
                "sns:CreateTopic",
                "iam:GetRole",
                "iam:GetInstanceProfile",
                "iam:ListRoles",
                "iam:ListInstanceProfiles"
            ]
     resources = [ 
        "*",
        ]
  }
    statement {
      effect = "Allow"
      actions = [
                "s3:ListBucket",
                "s3:GetObject"
            ]
      resources = [ 
        "arn:aws:s3:::*image-build*",
        ]
    }

    statement {
      effect = "Allow"
      actions = [
        "iam:PassRole"
            ]
      resources = [ 
        "*",
        ]
      condition {
        test     = "StringLike"
        variable = "iam:PassedToService"
        values =  [
            "ec2.amazonaws.com",
            "ssm.amazonaws.com"
        ]
      }
    }
}

resource "aws_ssoadmin_permission_set_inline_policy" "allowed_marketplace" {
  inline_policy      = data.aws_iam_policy_document.awsmarketplacefullaccess.json
  instance_arn       = aws_ssoadmin_permission_set.allowed_marketplace.instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.allowed_marketplace.arn
}