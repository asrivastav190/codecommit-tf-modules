output "root_id" {
  value = data.aws_organizations_organizational_units.ou.id
}

output "deny_marketplace_policy_id" {
  value = aws_organizations_policy.deny_marketplace_access.id
}

data "aws_caller_identity" "current" {}

output "scp_policy_account_id" {
  value = data.aws_caller_identity.current.account_id
}

output "sso_arn" {
  value = aws_ssoadmin_permission_set.allowed_marketplace.arn
}