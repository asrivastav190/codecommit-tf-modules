#Below Route Table will be associated with all Spoke VPC attachment and Ingress VPC attachemnt

resource "aws_ec2_transit_gateway_route_table" "spoke_vpc_inspection_tgw_route_table" {
  transit_gateway_id = aws_ec2_transit_gateway.iso_ne_transit_gateway.id
  tags               = merge(var.tags, { "Name" = "Spoke VPC Inspection Route Table" })
  provider           = aws.tgw
}

#Below Route Table will be associated with Firewall VPC attachment

resource "aws_ec2_transit_gateway_route_table" "firewall_subnet_tgw_route_table" {
  transit_gateway_id = aws_ec2_transit_gateway.iso_ne_transit_gateway.id
  tags               = merge(var.tags, { "Name" = "firewall_subnet_tgw_route_table" })
  provider           = aws.tgw
}

### This default route in Spoke Inspection TGW Route Table will route traffic generated from spoke VPC to Inspection VPC
resource "aws_ec2_transit_gateway_route" "default_inspection_vpc_route" {
  destination_cidr_block         = "0.0.0.0/0"
  transit_gateway_attachment_id  = var.inspection_vpc_tgw_attachement_id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.spoke_vpc_inspection_tgw_route_table.id
  provider                       = aws.tgw
}

###This default route in Firewall TGW Route Table will route traffic to central egress VPC from firewall VPC
resource "aws_ec2_transit_gateway_route" "default_egress_vpc_route" {
  destination_cidr_block         = "0.0.0.0/0"
  transit_gateway_attachment_id  = var.central_egress_vpc_attachement_id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.firewall_subnet_tgw_route_table.id
  provider                       = aws.tgw
}