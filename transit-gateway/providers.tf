#Below is the required version for aws provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.73.0"
      configuration_aliases = [ aws.tgw, aws.peertgw ]
    }
  }
}