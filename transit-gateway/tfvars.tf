# Variables declaration for Transit Gateway Creation. Below Input Varibales are expected to be passed form calling module

variable "transit_gateway_name" {
  description = "Transit gateway name in us-east-1 region"
  type        = string
}
variable "peer_transit_gateway_name" {
  type        = string
  description = "Peer transit gateway name in us-west-2 region"
}

variable "inspection_vpc_tgw_attachement_id" {
  type        = string
  description = "Transit Gateway VPC attachemnt identifier for Inspection VPC"
}

variable "central_egress_vpc_attachement_id" {
  type        = string
  description = "Transit gateway VPC attachement identifier for Central Egress VPC"
}
#Below Input Variables are declared and defined.These values are not expected to be passed from calling module.

variable "amazon_side_asn" {
  description = "Private Autonomous System Number (ASN) for the Amazon side of a BGP session"
  type        = string
  default     = "64512"
}

variable "enable_auto_accept_shared_attachments" {
  description = "Whether resource attachment requests are automatically accepted."
  type        = bool
  default     = true
}

variable "enable_default_route_table_association" {
  description = "Whether resource attachments are automatically associated with the default association route table"
  type        = bool
  default     = false
}

variable "enable_default_route_table_propagation" {
  description = "Whether resource attachments automatically propagate routes to the default propagation route table"
  type        = bool
  default     = true
}

variable "enable_dns_support" {
  description = "Whether DNS support is enabled"
  type        = bool
  default     = true
}

variable "enable_vpn_ecmp_support" {
  description = "Whether VPN Equal Cost Multipath Protocol support is enabled"
  type        = bool
  default     = true
}

variable "tags" {
  type = map(string)
}