
### Internet Gateway Ingress route table for Public workload vpc

resource "aws_route_table" "igw_ingress_rt" {
  vpc_id = aws_vpc.iso_ne_workload_vpc.id
  tags = merge(var.tags, { "Name" = "${var.vpc_name}-igw-ingress-rt" })
}

resource "aws_route_table_association" "igw_rt_association" {
  gateway_id      = aws_internet_gateway.igw.id
  route_table_id  = aws_route_table.igw_ingress_rt.id
}
### Firewall subnet route table and route table association

resource "aws_route_table" "nfw_subnet_rt" {
  vpc_id = aws_vpc.iso_ne_workload_vpc.id
  route {
    cidr_block         = "0.0.0.0/0"
    gateway_id         = aws_internet_gateway.igw.id
  }
  tags = merge(var.tags, { "Name" = "${var.vpc_name}-nfw-sn-rt" })
}

resource "aws_route_table_association" "workload_vpc_nfw_rt_association" {
  for_each       = var.nfw_subnets
  subnet_id      = aws_subnet.workload_vpc_nfw_subnet[each.key].id
  route_table_id = aws_route_table.nfw_subnet_rt.id
}
### Transit gateway subnet route table and route table assocaition

resource "aws_route_table" "tgw_subnet_rt" {
  vpc_id = aws_vpc.iso_ne_workload_vpc.id
  tags = merge(var.tags, { "Name" = "${var.vpc_name}-tgw-sn-rt" })
}

resource "aws_route_table_association" "workload_vpc_tgw_rt_association" {
  for_each       = var.tgw_subnets
  subnet_id      = aws_subnet.workload_vpc_tgw_subnet[each.key].id
  route_table_id = aws_route_table.tgw_subnet_rt.id
}

### Protected Public workload subnet route table

resource "aws_route_table" "public_workload_subnet_rt" {
  vpc_id = aws_vpc.iso_ne_workload_vpc.id
  tags = merge(var.tags, { "Name" = "${var.vpc_name}-public-wl-sn-rt" })
}


resource "aws_route_table_association" "workload_vpc_public_worklaod_rt_association" {
  for_each       = var.public_workload_subnets
  subnet_id      = aws_subnet.workload_vpc_public_workload_subnet[each.key].id
  route_table_id = aws_route_table.public_workload_subnet_rt.id
}
### Private workload subnet route table and association

resource "aws_route_table" "private_workload_subnet_rt" {
  vpc_id = aws_vpc.iso_ne_workload_vpc.id
  route {
    cidr_block         = "0.0.0.0/0"
    transit_gateway_id = var.transit_gateway_id
  }
  tags = merge(var.tags, { "Name" = "${var.vpc_name}-private-wl-sn-rt" })
}

resource "aws_route_table_association" "workload_vpc_private_worklaod_rt_association" {
  for_each       = var.private_workload_subnets
  subnet_id      = aws_subnet.workload_vpc_private_workload_subnet[each.key].id
  route_table_id = aws_route_table.private_workload_subnet_rt.id
}
