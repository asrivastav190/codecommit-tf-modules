resource "aws_ec2_transit_gateway_vpc_attachment" "spoke_vpc_attachment" {
  subnet_ids                                      = [for subnet in aws_subnet.workload_vpc_tgw_subnet : subnet.id]
  transit_gateway_id                              = var.transit_gateway_id
  vpc_id                                          = aws_vpc.iso_ne_workload_vpc.id
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
}

# Associate the above VPC attachment with the Route Table of Transit Gateway

resource "aws_ec2_transit_gateway_route_table_association" "tgw_rt_association" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.spoke_vpc_attachment.id
  transit_gateway_route_table_id = var.spoke_vpc_inspection_tgw_route_table_id
}

# Propagate the Route for the destination VPC CIDR in the Route Table

resource "aws_ec2_transit_gateway_route_table_propagation" "tgw_rt_propagation" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.spoke_vpc_attachment.id
  transit_gateway_route_table_id = var.firewall_subnet_tgw_route_table_id
}