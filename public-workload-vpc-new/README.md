# Workload/Spoke VPC with Public Subnets Protected by Network Firewall
## Description

The requirement is to create a VPC with below components:

-General private workload subnets spread across 4 availability zones having /24 CIDR.  

-General Public workload subnets spread acroos 4 availability zaones having /24 CIDR.

-Dedicated Transit Gateway attachment subnets with /28 CIDR.  

-Dedicated Network firewall subnets with /28 CIDR to host AWS netwrok firewall endpoints.

-All ingress traffic must be routed through the network firewall hosted within vpc.

-All egress traffic will pass through the egress vpc after going through firewall in inspection vpc.

-VPC Flow log’s need to be enabled.  

-A S3 endpoint for each VPC.  

-As per initial deployment, we are creating the two workload VPCs.  
```
    --Public Development VPC  
    --Public Production VPC  
```   

## Terraform Module
```
\---workload-vpc
    |   igw.tf
    |   flow_log.tf
    |   s3-endpoint.tf
    |   outputs.tf
    |   providers.tf
    |   README.md
    |   tfvars.tf
    |   subnets.tf
    |   route_table.tf
    |   vpc-tgw-attachment.tf
    |   vpc.tf
```



This workload VPC module will be called from main.tf in the root directory as shown below.

```
module "create_public_workload_vpc" {
  providers = {
    aws = aws.use1
  }
  source                                  = "bitbucket.org/iso-ne/terraform-modules/workload-vpc"
  for_each                                = var.public_workload_vpc
  vpc_name                                = each.value["vpc_name"]
  cidr_ab                                 = each.value["cidr_ab"]
  flow_log_enable                         = each.value["flow_log_enable"]
  tgw_subnets                             = each.value["tgw_subnets"]
  nfw_subnets                             = each.value["nfw_subnets"]
  private_workload_subnets                = each.value["private_workload_subnets"]
  public_workload_subnets                 = each.value["public_workload_subnets"]
  map_public_ip_on_launch                 = var.map_public_ip_on_launch
  s3_bucket_arn                           = module.create_s3.s3_bucket_arn
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
  tags                                    = var.tags
}
 
## Root Structure
User Inputs: 
The below User Inputs are required and can be customized from the variables.tf file in the root directory.
```
###Workload vpc name, cidr, and flow log enable option. 
variable "public_workload_vpc" {
  type = map(object({
    vpc_name        = string
    cidr_ab         = string
    flow_log_enable = bool
    tgw_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
    nfw_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
    private_workload_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
    public_workload_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
  }))
  default = {
    development_vpc = {
      cidr_ab         = "10.167.0.0/16"
      vpc_name        = "public-dev-vpc"
      flow_log_enable = false
      public_workload_subnets = {
        subnet1 = {
          cidr_block = "10.167.17.0/24"
          name       = "pub-dev-vpc-pub-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.167.18.0/24"
          name       = "pub-dev-vpc-pub-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.167.19.0/24"
          name       = "pub-dev-vpc-pub-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.167.20.0/24"
          name       = "pub-dev-vpc-pub-workload-sn-4"
          az         = "use1-az2"
        }
      }
      tgw_subnets = {
        subnet1 = {
          cidr_block = "10.167.0.112/28"
          name       = "pub-dev-vpc-tgw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.167.0.128/28"
          name       = "pub-dev-vpc-tgw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.167.0.144/28"
          name       = "pub-dev-vpc-tgw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.167.0.160/28"
          name       = "pub-dev-vpc-tgw-sn-4"
          az         = "use1-az2"
        }
      }
      nfw_subnets = {
        subnet1 = {
          cidr_block = "10.167.0.176/28"
          name       = "pub-dev-vpc-nfw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.167.0.192/28"
          name       = "pub-dev-vpc-nfw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.167.0.208/28"
          name       = "pub-dev-vpc-nfw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.167.0.224/28"
          name       = "pub-dev-vpc-nfw-sn-4"
          az         = "use1-az2"
        }
      }
      private_workload_subnets = {
        subnet1 = {
          cidr_block = "10.167.21.0/24"
          name       = "pub-dev-vpc-prv-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.167.22.0/24"
          name       = "pub-dev-vpc-prv-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.167.23.0/24"
          name       = "pub-dev-vpc-prv-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.167.24.0/24"
          name       = "pub-dev-vpc-prv-workload-sn-4"
          az         = "use1-az2"
        }
      }
    }
    production_vpc = {
      cidr_ab         = "10.168.0.0/16"
      vpc_name        = "public-prod-vpc"
      flow_log_enable = false
      public_workload_subnets = {
        subnet1 = {
          cidr_block = "10.168.7.0/24"
          name       = "public-prod-vpc-pub-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.168.8.0/24"
          name       = "public-prod-vpc-pub-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.168.9.0/24"
          name       = "public-prod-vpc-pub-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.168.10.0/24"
          name       = "public-prod-vpc-pub-workload-sn-4"
          az         = "use1-az2"
        }
      }
      tgw_subnets = {
        subnet1 = {
          cidr_block = "10.168.0.112/28"
          name       = "public-prod-vpc-tgw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.168.0.128/28"
          name       = "public-prod-vpc-tgw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.168.0.144/28"
          name       = "public-prod-vpc-tgw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.168.0.160/28"
          name       = "public-prod-vpc-tgw-sn-4"
          az         = "use1-az2"
        }
      }
      nfw_subnets = {
        subnet1 = {
          cidr_block = "10.168.0.176/28"
          name       = "pub-prod-vpc-nfw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.168.0.192/28"
          name       = "pub-prod-vpc-nfw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.168.0.208/28"
          name       = "pub-prod-vpc-nfw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.168.0.224/28"
          name       = "pub-prod-vpc-nfw-sn-4"
          az         = "use1-az2"
        }
      }
      private_workload_subnets = {
        subnet1 = {
          cidr_block = "10.168.21.0/24"
          name       = "pub-prod-vpc-prv-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.168.22.0/24"
          name       = "pub-prod-vpc-prv-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.168.23.0/24"
          name       = "pub-prod-vpc-prv-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.168.24.0/24"
          name       = "pub-prod-vpc-prv-workload-sn-4"
          az         = "use1-az2"
        }
        # Add subnet block here for another private workload subnet
      }
    }
    # Add vpc block here for another vpc
  }
}
``` 

VPC Name and VPC CIDR: Provide a VPC name and CIDR mapping in the variable public_workload_vpc. As part of initial deployment, two VPCs are being created. If any new VPCs are required the same can be added in the variable map.

***Remember to update this variable as per mapping in the account***
![image1](../images/workload-vpc/az_mapping.PNG)

## Automated Inputs: 
Below are inputs passed into the workload_vpc_tgw_attachment module from the outputs of transit gateway module.

spoke_vpc_inspection_tgw_route_table_id: This Transit Gateway route table is associated with the Transit Gateway and spoke VPC attachment. The attachment creation can be found in vpc-tgw-attachement.tf in the workload-vpc module.

firewall_subnet_tgw_route_table_id: This route table is used to propagate the spoke VPC CIDR route to its Transit Gateway route table. 
transit_gateway_id: The ID for the Transit Gateway in the us-east-1 region is passed to attach the Transit Gateway with the created spoke VPC.
```
## Core Module: The core workload/spoke VPC module is structured as shown below.
```
\---workload-vpc
    |   flow_log.tf
    |   vpc-tgw-attachment.tf
    |   outputs.tf
    |   providers.tf
    |   README.md
    |   tfvars.tf
    |   subnets.tf
    |   route-table.tf
    |   vpc.tf
    |   s3-endpoint.tf
```
Resources
VPC: A VPC with the passed CIDR range and Name, s3 endpoint and flow log enabled.

Subnets: Below four types of subnets will be created.

        - Dedicated transit gateway subnets for transit gateway attachment, having /28 CIDR
        - Dedicated netwrok firewall subnets for network firewall endpoints, having /28 CIDR
        - Public workload subnets to host ALB etc, having /24 CIDR.
        - Private worklaod  subnets to host private workload, having /24 CIDR.

Route Table: The route table will be created for each type of subnets mentioned above. One additional route table "igw_ingress_rt" will be created and associated with Internet gatewat that will route traffic to respective firewall endpoint ine ach az.

S3 Endpoint : One S3 endpoint will,be created along with VPC and all private subnets in vpc will have internal S3 connectivity.

VPC Flow Log : Based on the flow_log_enable option in varibales.tf, vpc flow logs will be enabled for vpc having s3 as destination log bucket.

VPC-Transit Gateway attachment: The newly created spoke/workload VPC will be attached to the transit gateway and a VPC CIDR route will be propagated to the spoke_vpc_inspection_tgw_route_table.
The firewall_subnet_tgw_route_table will be associated with this VPC -Transit Gateway attachment.
#### Diagram
This module implenments the below VPC architecture.

![image2](../images/public-workload-vpc/publicVPCarch.PNG)

#### Reference Links
N/A

#### Limitations
N/A
