## Workload vpc transit gateway subnets
resource "aws_subnet" "workload_vpc_tgw_subnet" {
  for_each                = var.tgw_subnets
  vpc_id                  = aws_vpc.iso_ne_workload_vpc.id
  availability_zone_id    = each.value.az
  cidr_block              = each.value.cidr_block
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", each.value.name)
    }
  )
}
## Workload vpc network firewall subnets
resource "aws_subnet" "workload_vpc_nfw_subnet" {
  for_each                = var.nfw_subnets
  vpc_id                  = aws_vpc.iso_ne_workload_vpc.id
  availability_zone_id    = each.value.az
  cidr_block              = each.value.cidr_block
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", each.value.name)
    }
  )
}

## Workload vpc Public Workload subnets
resource "aws_subnet" "workload_vpc_public_workload_subnet" {
  for_each                = var.public_workload_subnets
  vpc_id                  = aws_vpc.iso_ne_workload_vpc.id
  availability_zone_id    = each.value.az
  cidr_block              = each.value.cidr_block
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", each.value.name)
    }
  )
}

## Workload VPC private workload subnets
resource "aws_subnet" "workload_vpc_private_workload_subnet" {
  for_each                = var.private_workload_subnets
  vpc_id                  = aws_vpc.iso_ne_workload_vpc.id
  availability_zone_id    = each.value.az
  cidr_block              = each.value.cidr_block
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", each.value.name)
    }
  )
}