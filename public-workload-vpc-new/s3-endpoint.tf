######################
# VPC Endpoint for S3
######################

data "aws_vpc_endpoint_service" "s3" {
  service      = "s3"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id       = aws_vpc.iso_ne_workload_vpc.id
  service_name = data.aws_vpc_endpoint_service.s3.service_name
  tags         = merge(var.tags, { "Name" = format("%s-%s", var.vpc_name, "s3-endpoint")})
}

resource "aws_vpc_endpoint_route_table_association" "private_s3_workload_rt" {
  vpc_endpoint_id = aws_vpc_endpoint.s3.id
  route_table_id  = aws_route_table.private_workload_subnet_rt.id
}

resource "aws_vpc_endpoint_route_table_association" "private_s3_tgw_rt" {
  vpc_endpoint_id = aws_vpc_endpoint.s3.id
  route_table_id  = aws_route_table.tgw_subnet_rt.id
}