output "vpc_id" {
  value       = aws_vpc.iso_ne_workload_vpc.id
  description = "The ID of the VPC"
}

output "workload_vpc_tgw_subnet" {
    value = [ for subnet in aws_subnet.workload_vpc_tgw_subnet : subnet.id]
}

output "workload_vpc_nfw_subnet" {
    value = [ for subnet in aws_subnet.workload_vpc_nfw_subnet : subnet.id]
}

output "workload_vpc_public_workload_subnet" {
    value = [ for subnet in aws_subnet.workload_vpc_public_workload_subnet : subnet.id]
}

output "workload_vpc_private_workload_subnet" {
    value = [ for subnet in aws_subnet.workload_vpc_private_workload_subnet : subnet.id]
}

output "igw_ingress_rt" {
  value       = aws_route_table.igw_ingress_rt.id
  description = "Workload VPC route table identifier"
}

## arn
output "workload_vpc_public_workload_subnet_arn" {
    value = [ for subnet in aws_subnet.workload_vpc_public_workload_subnet : subnet.arn]
}

output "workload_vpc_private_workload_subnet_arn" {
    value = [ for subnet in aws_subnet.workload_vpc_private_workload_subnet : subnet.arn]
}