## Create workload vpc Internet gateway 
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.iso_ne_workload_vpc.id
  tags   = merge(var.tags, { "Name" = format("%s-igw", var.vpc_name) })
}