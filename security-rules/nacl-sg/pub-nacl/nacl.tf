# /*
# This terraform template will create the following resources,
# 1. NACL
# */

# ## Inspection Private NACL
resource "aws_network_acl" "public" {
  vpc_id     = var.vpc_id
  subnet_ids = var.public_subnets
  tags       = merge(var.tags, { "Name" = "${var.vpc_name}-NACL-Public" })
}

resource "aws_network_acl_rule" "private" {
  network_acl_id = aws_network_acl.public.id
  for_each       = var.public_nacl_rules
  rule_action    = each.value.rule_action
  rule_number    = each.value.rule_number
  from_port      = each.value.from_port
  to_port        = each.value.to_port
  protocol       = each.value.protocol
  cidr_block     = each.value.cidr_block
  egress         = each.value.egress
}



# resource "aws_network_acl" "private_tgw_ingress" {
#   vpc_id     = aws_vpc.ingress_vpc.id
#   subnet_ids = [for value in aws_subnet.prv_tgw_ingress : value.id]
#   tags       = merge(var.tags, { "Name" = "${local.ingress_env}-NACL-Private" })
# }

# resource "aws_network_acl_rule" "ingress_private_rules" {
#   network_acl_id = aws_network_acl.private_tgw_ingress.id
#   for_each       = var.private_nacl_rules
#   rule_action    = each.value.rule_action
#   rule_number    = each.value.rule_number
#   from_port      = each.value.from_port
#   to_port        = each.value.to_port
#   protocol       = each.value.protocol
#   cidr_block     = each.value.cidr_block
#   egress         = each.value.egress
# }

# # ## Egress public & private NACL
# resource "aws_network_acl" "public_egress" {
#   vpc_id     = aws_vpc.egress_vpc.id
#   subnet_ids = [for value in aws_subnet.pub_egress : value.id]
#   tags       = merge(var.tags, { "Name" = "${local.egress_env}-NACL-Public" })
# }

# resource "aws_network_acl_rule" "egress_public_rules" {
#   network_acl_id = aws_network_acl.public_egress.id
#   for_each       = var.public_nacl_rules
#   rule_action    = each.value.rule_action
#   rule_number    = each.value.rule_number
#   from_port      = each.value.from_port
#   to_port        = each.value.to_port
#   protocol       = each.value.protocol
#   cidr_block     = each.value.cidr_block
#   egress         = each.value.egress
# }

# resource "aws_network_acl" "private_tgw_egress" {
#   vpc_id     = aws_vpc.egress_vpc.id
#   subnet_ids = [for value in aws_subnet.prv_tgw_egress : value.id]
#   tags       = merge(var.tags, { "Name" = "${local.egress_env}-NACL-Private" })
# }

# resource "aws_network_acl_rule" "egress_private_rules" {
#   network_acl_id = aws_network_acl.private_tgw_egress.id
#   for_each       = var.private_nacl_rules
#   rule_action    = each.value.rule_action
#   rule_number    = each.value.rule_number
#   from_port      = each.value.from_port
#   to_port        = each.value.to_port
#   protocol       = each.value.protocol
#   cidr_block     = each.value.cidr_block
#   egress         = each.value.egress
# }