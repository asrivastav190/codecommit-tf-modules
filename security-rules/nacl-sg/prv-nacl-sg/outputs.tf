output "private_nacl_id" {
  description = "Private NACL ID of $${var.vpc_name}"
  value = aws_network_acl.private.id
}

output "sg_id" {
  description = "Security group of $${var.vpc_name}"
  value = aws_default_security_group.default.id
}