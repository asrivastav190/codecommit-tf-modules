variable "vpc_id" {}
variable "vpc_name" {}
variable "private_subnets" {}
variable "tags" {
  type = map(string)
}
variable "private_nacl_rules" {
  type = map(map(string))
}
variable "ingress_rules" {}
variable "egress_rules" {}