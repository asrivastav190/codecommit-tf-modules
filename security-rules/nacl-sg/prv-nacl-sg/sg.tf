resource "aws_default_security_group" "default" {
  vpc_id      = var.vpc_id

  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port        = ingress.value.from_port
      to_port          = ingress.value.to_port
      protocol         = ingress.value.protocol
      description      = ingress.value.description
      cidr_blocks      = [ingress.value.cidr_blocks]
      ipv6_cidr_blocks = [ingress.value.ipv6_cidr_blocks]
      self             = ingress.value.self
    }
  }

  dynamic "egress" {
    for_each = var.egress_rules
    content {
      from_port        = egress.value.from_port
      to_port          = egress.value.to_port
      protocol         = egress.value.protocol
      description      = egress.value.description
      cidr_blocks      = [egress.value.cidr_blocks]
      ipv6_cidr_blocks = [egress.value.ipv6_cidr_blocks]
      self             = egress.value.self
    }
  }

  tags = merge(var.tags, { "Name" = "${var.vpc_name}-SG" })
}
