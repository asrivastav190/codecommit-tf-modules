# AWS Network Firewall for public/private workload VPC.
### Service: AWS Network Firewall

#### Description
AWS Network Firewall inspects and helps to control VPC-to-VPC traffic to logically separate networks hosting sensitive applications or line-of-business workloads and is deployed in the central inspection VPC to have traffic inspected. It can provide fine-grained network security controls for interconnected VPCs using [AWS Transit Gateway](https://aws.amazon.com/transit-gateway/). 

For a detailed explanation on AWS Network Firewall, please refer to the link below:

https://docs.aws.amazon.com/network-firewall/latest/developerguide/what-is-aws-network-firewall.html

## Terraform Module
```
+---NetworkFirewall
|       locals.tf
|       nfw.tf
|       nfw_route.tf
|       outputs.tf
|       policy.tf
|       rulegroup.tf
|       tfvars.tf
```

data.tf: This terraform file is used to fetch public share route table IDs , public share subnet cidr blocks and public network firewall subnet IDs to update firewall endpoint route and attach network firewall to its appropriate subnets.

nfw.tf: This Terraform file is used to create a network firewall for each workload VPC. It creates a firewall endpoint in each firewall subnet.

nfw_route.tf: This file is responsible for retrieving the firewall route table and associating the firewall endpoint to it.

outputs.tf: This is responsible for printing the output of the firewall ID and endpoints.

policy.tf: This is required for the firewall policy and associates stateful and stateless rule groups to it.

rulegroups.tf: This file is responsible for creating both stateless and stateful rule groups.

tfvars.tf: This Terraform file is used to pass input variables like the workload VPC CIDR range, tags, stateless and stateful rule group rules which in turn is provided by the root folder variables.tf file.

## Root Structure
```
 ---security-root
     |   main.tf
     |   outputs.tf
     |   variables.tf
```

main.tf: This file is responsible for passing variables required to call a network firewall module and create its resources as defined above in a specified AWS account.

![Image](../images/security-rules/pub-prv-workload-vpc-nfw-main.PNG)

variables.tf: Variables passed specifically for network firewall modules are environment names  used as part of tags, stateless and stateful rule groups. Below is the snapshot for reference:

![Image](../images/security-rules/workload-vpc-nfw-variables.png)

Please note variables stateless_rule_group and stateful_rule_group can be modified as per the requirement table displayed on the next page.









```
|Name|	|Type|	|Description|	|Required?|	|Value|	|Notes|
|:------------|:------------|:------------|:------------|:------------|:------------|
|workload_env|	|string|	|Environment name|	|Yes|	|WorkloadVPC|	|Name of the VPC where network firewall is deployed|
|stateless_rule_group|	|map of strings|	|Set of Stateless Rule Groups|	|Yes|	|"ephemeral-port-rule": {"capacity":100, "priority": 1, "actions": "aws:drop","source_from_port": 1024 , "source_to_port": 65535 , "protocol": 6, "source_address": "0.0.0.0/0" ,"destination_from_port": 1024, "destination_to_port": 65535, "destination_address": "0.0.0.0/0"},|	|RuleName along with rule capacity , priority, action , source details , protocol and destination details|
|stateful_rule_group|	|map of strings|	|Set of Stateful Rule Groups|	|Yes|	|"drop-icmp": {"capacity":100, "action": "DROP", "destination": "0.0.0.0/0", "destination_port": 1, "direction": "ANY", "protocol": "TCP", "source": "0.0.0.0/0", "source_port": 1},

"ssh-rule": {"capacity":100, "action": "DROP", "destination": "0.0.0.0/0", "destination_port": 22, "direction": "ANY", "protocol": "TCP", "source": "0.0.0.0/0", "source_port": 22},

"http-rule": {"capacity":100, "action": "DROP", "destination": "0.0.0.0/0", "destination_port": 80, "direction": "ANY", "protocol": "TCP", "source": "0.0.0.0/0", "source_port": 80},

"https-rule": {"capacity":100, "action": "DROP", "destination": "0.0.0.0/0", "destination_port": 443, "direction": "ANY", "protocol": "TCP", "source": "0.0.0.0/0", "source_port": 443},|	|RuleName along with rule capacity , action , destination details ,direction of traffic flow to inspect, protocol and source details|
 ```
 
#### Reference Links
Stateless Rule Group:

https://docs.aws.amazon.com/network-firewall/latest/developerguide/stateless-rule-groups-5-tuple.html

Stateful Rule Group:

https://docs.aws.amazon.com/network-firewall/latest/developerguide/stateful-rule-groups-ips.html


#### Diagram

![Image](../images/security-rules/workload-vpc-nfw-arch.PNG)
  
#### Limitations
1.)	Adding AWS managed rules to the network firewall policy for attaining protection against common application vulnerabilities or other unwanted traffic, without having to write your own rules. This can be achieved from the AWS console, but is not supported by Terraform. See the reference link to enable it from the console: 
https://docs.aws.amazon.com/network-firewall/latest/developerguide/aws-managed-rule-groups.html

---
___

# AWS Network Firewall for Inspection VPC
### Service: AWS Network Firewall
#### Description
AWS Network Firewall inspects and helps to control VPC-to-VPC traffic to logically separate networks hosting sensitive applications or line-of-business workloads and is deployed in the central inspection VPC to have traffic inspected. It can provide fine-grained network security controls for interconnected VPCs using AWS Transit Gateway. 

For a detailed explanation on AWS Network Firewall, please refer to the link below:

https://docs.aws.amazon.com/network-firewall/latest/developerguide/what-is-aws-network-firewall.html

#### Terraform Module
```
 +---security-rules
 |   +---inspection-network-firewall
 |   |       locals.tf
 |   |       nfw.tf
 |   |       nfw_route.tf
 |   |       outputs.tf
 |   |       policy.tf
 |   |       providers.tf
 |   |       rulegroup.tf
 |   |       tfvars.tf
```

locals.tf: Locals are named values that you can refer to in your configuration. In this case, we are passing tag values for each resource in the network firewall module.

nfw.tf: This Terraform file is used to create a network firewall in Inspection VPC. It creates a firewall endpoint in each firewall subnet.

nfw_route.tf: This file is responsible for retrieving the firewall route table and associating the firewall endpoint to it.

outputs.tf: This is responsible for printing the output of the firewall ID and endpoints.

policy.tf: This is required for the firewall policy and associates stateful and stateless rule groups to it.

rulegroups.tf: This file is responsible for creating both stateless and stateful rule groups.

tfvars.tf: This Terraform file is used to pass input variables like the Inspection VPC CIDR range, tags, stateless and stateful rule group rules which in turn is provided by the root folder variables.tf file.

#### Root Structure
```
 ---security-root
    |   main.tf
    |   outputs.tf
    |   variables.tf
```

main.tf: This file is responsible for passing variables required to call a network firewall module and create its resources as defined above in a specified AWS account.
![Image](../images/security-rules/inspection-nfw-main.PNG)

variables.tf: Variables passed specifically for network firewall modules are environment names  used as part of tags, stateless and stateful rule groups. Below is the snapshot for reference:

![Image](../images/security-rules/inspection-nfw-variables.png)

Please note variables stateless_rule_group and stateful_rule_group can be modified as per the requirement table displayed below.

 
```
|Name|	|Type|	|Description|	|Required?|	|Value|	|Notes|
|:------------|:------------|:------------|:------------|:------------|:------------|
|inspection_env|	|string|	|Environment name|	|Yes|	|InspectionVPC|	|Name of the VPC where network firewall is deployed|
|stateless_rule_group|	|map of strings|	|Set of Stateless Rule Groups|	|Yes|	|"ephemeral-port-rule": {"capacity":100, "priority": 1, "actions": "aws:drop","source_from_port": 1024 , "source_to_port": 65535 , "protocol": 6, "source_address": "0.0.0.0/0" ,"destination_from_port": 1024, "destination_to_port": 65535, "destination_address": "0.0.0.0/0"},|	|RuleName along with rule capacity , priority, action , source details , protocol and destination details|
|stateful_rule_group|	|map of strings|	|Set of Stateful Rule Groups|	|Yes|	|"drop-icmp": {"capacity":100, "action": "DROP", "destination": "0.0.0.0/0", "destination_port": 1, "direction": "ANY", "protocol": "TCP", "source": "0.0.0.0/0", "source_port": 1},

"ssh-rule": {"capacity":100, "action": "DROP", "destination": "0.0.0.0/0", "destination_port": 22, "direction": "ANY", "protocol": "TCP", "source": "0.0.0.0/0", "source_port": 22},

"http-rule": {"capacity":100, "action": "DROP", "destination": "0.0.0.0/0", "destination_port": 80, "direction": "ANY", "protocol": "TCP", "source": "0.0.0.0/0", "source_port": 80},

"https-rule": {"capacity":100, "action": "DROP", "destination": "0.0.0.0/0", "destination_port": 443, "direction": "ANY", "protocol": "TCP", "source": "0.0.0.0/0", "source_port": 443},|	|RuleName along with rule capacity , action , destination details ,direction of traffic flow to inspect, protocol and source details|
 ```
 
#### Reference Links
Stateless Rule Group:
https://docs.aws.amazon.com/network-firewall/latest/developerguide/stateless-rule-groups-5-tuple.html

Stateful Rule Group:

https://docs.aws.amazon.com/network-firewall/latest/developerguide/stateful-rule-groups-ips.html

#### Diagram

![Image](../images/security-rules/inspection-arch-snap.png)
  
#### Limitations
1.)	Adding AWS managed rules to the network firewall policy for attaining protection against common application vulnerabilities or other unwanted traffic, without having to write your own rules. This can be achieved from the AWS console, but is not supported by Terraform. See the reference link to enable it from the console: 
https://docs.aws.amazon.com/network-firewall/latest/developerguide/aws-managed-rule-groups.html

---
___

# AWS NACL & SG

#### Description
This section define access control list and security group rules for all VPCs.

Reference links:
NACL:
https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html

Security Group:
https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html#VPCSecurityGroups
#### Pre-requities:
**Need to run first deploy all resources in root folder in order to fill in variables.tf file for security.** module.

##  Terraform Module
```
 |   +---nacl-sg
 |   |   +---prv-nacl-sg
 |   |   |       nacl.tf
 |   |   |       outputs.tf
 |   |   |       providers.tf
 |   |   |       sg.tf
 |   |   |       tfvars.tf
 |   |   |
 |   |   \---pub-nacl
 |   |           nacl.tf
 |   |           outputs.tf
 |   |           providers.tf
 |   |           tfvars.tf
```

Folder prv-nacl-sg contains below files:

nacl.tf: Used to create a private NACL for all VPCs (Inspection , Ingress , Egress , Private workload , Public/Private workload)

sg.tf: Defines the allowed security groups required for all VPCs (Inspection , Ingress , Egress , Private workload , Public/Private workload)

outputs.tf: This is responsible for printing private nacl ids and security group ids created for all VPCs (Inspection , Ingress , Egress , Private workload , Public/Private workload).

tfvars.tf: This Terraform file is used to pass input variables like all VPC IDs, vpc names, priavte nacl rules, security group rules , private subnets and tags which in turn is provided by the root folder variables.tf file.

Folder pub-nacl contains below files:

nacl.tf: Used to create a public NACL for all VPCs except Inspection VPC ( Ingress , Egress , Private workload , Public/Private workload).

outputs.tf: This is responsible for printing public nacl ids.

tfvars.tf: This Terraform file is used to pass input variables like all VPC IDs, vpc names, public nacl rules , public subnets and tags which in turn is provided by the root folder variables.tf file.

## Root Structure
```
 ---security-root
    |   main.tf
    |   outputs.tf
    |   variables.tf
```

main.tf: This file is responsible for passing variables required to call  nacl-sg module and create its resources as defined above in a specified AWS account.
![Image](../images/security-rules/nacl-main.png)

variables.tf: Variables passed specifically for security-rules modules are tags, stateless & stateful rule groups , public & private nacl rules , security group ingress & egress rules , vpc & subnet details mapping to create nacls etc. Below is the snapshot for reference:

![Image](../images/security-rules/prv-nacl-vars.PNG)
**NOTE: NACL rules can be modified at this place if any modification or new rule is required.**

Below screenshot shows mapping of vpc id & name and private subnets. These could be obtained by running **terraform output** in root folder.

![Image](../images/security-rules/prv-nacl-vpc-subnet-map.PNG)

Below screenshot is refrence of variables used to create public nacl rules and vpc_pub_subnets is used to map vpc id&name and public subnets to which nacl will be attached to.
![Image](../images/security-rules/pub-nacl-vars.PNG)
Below screenshot shows variables required to build network firewall for inspection vpc and public/private workload vpcs..
![Image](../images/security-rules/nfw-vars-1.PNG)
![Image](../images/security-rules/nfw-vars-2.PNG)
![Image](../images/security-rules/nfw-vars-3.PNG)

