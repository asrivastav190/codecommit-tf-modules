variable "vpc_id" {
  description = " VPC ID"
}

variable "vpc_name" {   type        = string }

variable "nfw_sub_suffix" {
  description = "Network firewall subnet IDs"
}

variable "prv_share_sub_suffix" {
  description = "Private Share subnet IDs"
}

variable "pub_sub_suffix" {}

variable "tags" {
  type = map(string)
}

variable "stateless_rule_group" {
  type    = map(map(string))
  default = {}
}

variable "stateful_rule_group" {
  type    = map(map(string))
  default = {}
}

variable "az" {
  type        = set(string)
  description = "List of availability zone to be initialized"
}

variable "pub_vpce_rt_id" {}
