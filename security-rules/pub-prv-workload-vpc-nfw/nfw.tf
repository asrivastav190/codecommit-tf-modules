resource "aws_networkfirewall_firewall" "public_workload_vpc_nfw" {
  name                = "${local.workload_env}-NetworkFirewall"
  firewall_policy_arn = aws_networkfirewall_firewall_policy.nfw_policy.arn
  vpc_id              = var.vpc_id

  dynamic "subnet_mapping" {
    for_each = data.aws_subnet_ids.pub_nfw_subnet.ids

    content {
      subnet_id = subnet_mapping.value
    }
  }
  tags = var.tags
}
