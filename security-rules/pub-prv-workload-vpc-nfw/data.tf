## Fetch subnet id
data "aws_subnet_ids" "pub_share" {
  vpc_id = var.vpc_id
  tags = {
    Name = "*-${var.pub_sub_suffix}-*"
  }
}

data "aws_subnet" "pub_share" {
  count = length(tolist(data.aws_subnet_ids.pub_share.ids))
  id    = element(tolist(data.aws_subnet_ids.pub_share.ids), count.index)
}

output "pub_share_cidr_block" {
  value = [for value in data.aws_subnet.pub_share : value.cidr_block]
}

## Fetch route table for firewall endpoint to be added.
data "aws_route_table" "pub_share" {
  subnet_id = element(tolist(data.aws_subnet_ids.pub_share.ids), 0)
}

## Fetch nfw subnets for nfw subnet mapping
data "aws_subnet_ids" "pub_nfw_subnet" {
  vpc_id = var.vpc_id

  tags = {
    Name = "*-${var.nfw_sub_suffix}-*"
  }
}

# ##Fetch pub share subnet cidr
data "aws_subnet_ids" "pub_share_subnet" {
  for_each = var.az
  vpc_id   = var.vpc_id

  tags = {
    #Name = "*-${var.pub_sub_suffix}-${upper(each.value)}"
    Name = "*-${var.pub_sub_suffix}-*"
  }
}

data "aws_route_table" "pub_share_subnet" {
  for_each  = var.az
  subnet_id = element(tolist(data.aws_subnet_ids.pub_share_subnet[each.value].ids), 0)
}

output "rt_pub_share_zones" {
  value = [for v in data.aws_route_table.pub_share_subnet : v.id]
}

