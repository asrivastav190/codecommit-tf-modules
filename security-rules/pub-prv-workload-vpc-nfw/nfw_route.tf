##Add firewall endpoint to public firewall route table
resource "aws_route" "pub_vpce_route" {
  count                  = length([for s in data.aws_subnet.pub_share : s.cidr_block])
  route_table_id         = var.pub_vpce_rt_id
  destination_cidr_block = element([for s in data.aws_subnet.pub_share : s.cidr_block], count.index)
  vpc_endpoint_id        = element([for value in flatten(aws_networkfirewall_firewall.public_workload_vpc_nfw[*].firewall_status[0].sync_states) : value.attachment[0].endpoint_id], count.index)
}

resource "aws_route" "pub_route" {
  count                  = length([for v in data.aws_route_table.pub_share_subnet : v.id])
  route_table_id         = element([for v in data.aws_route_table.pub_share_subnet : v.id], count.index)
  destination_cidr_block = "0.0.0.0/0"
  vpc_endpoint_id        = element([for value in flatten(aws_networkfirewall_firewall.public_workload_vpc_nfw[*].firewall_status[0].sync_states) : value.attachment[0].endpoint_id], 0)
}
