
# ## Network Firewall Policy
resource "aws_networkfirewall_firewall_policy" "nfw_policy" {
  name = "${local.inspection_env}-firewall-policy"
  firewall_policy {
    stateless_default_actions          = ["aws:forward_to_sfe"]
    stateless_fragment_default_actions = ["aws:forward_to_sfe"]

    dynamic "stateless_rule_group_reference" {
      for_each = var.stateless_rule_group

      content {
        priority     = stateless_rule_group_reference.value["priority"]
        resource_arn = aws_networkfirewall_rule_group.stateless[stateless_rule_group_reference.key].arn
      }
    }

    dynamic "stateful_rule_group_reference" {
      for_each = var.stateful_rule_group

      content {
        resource_arn = aws_networkfirewall_rule_group.stateful[stateful_rule_group_reference.key].arn
      }
    }
  }
}