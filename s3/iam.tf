resource "aws_iam_role" "replication" {
  name               = "tf-iam-role-replication-123456"
  assume_role_policy = data.template_file.assume_role_policy.rendered
}

resource "aws_iam_policy" "replication" {
  name   = "tf-iam-role-policy-replication-12345"
  policy = data.template_file.replication_policy.rendered
}

resource "aws_iam_role_policy_attachment" "replication" {
  role       = aws_iam_role.replication.name
  policy_arn = aws_iam_policy.replication.arn
}

