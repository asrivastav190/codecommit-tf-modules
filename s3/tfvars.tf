// Module: Terraform S3 Bucket

# Required
# --------

variable "aws_region" {
  description = "AWS region"
  #default     = "us-east-1"
}

# Optional
# --------

variable "bucket" {
  description = "Bucket name"
}

variable "log_bucket" {
  description = "log_Bucket name"
}

variable "replica_log_bucket" {
  description = "replica_Bucket name"
}

# https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl
variable "acl" {
  description = "Set canned ACL on bucket. Valid values are private, public-read, public-read-write, aws-exec-read, authenticated-read, bucket-owner-read, bucket-owner-full-control, log-delivery-write"
  default     = "private"
}

variable "force_destroy" {
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error"

}


variable "prevent_destroy" {
  description = "lifecycle rule to prevent the removal of a bucket during a destroy"
  default     = false
}


# Default server side encryption configuration

variable "sse_algorithm" {
  description = "The server-side encryption algorithm to use. Valid values are AES256 and aws:kms"
  default     = "AES256"
}

# lifecycle rule - abort multi-part uploads

variable "enable_abort_incomplete_multipart_upload" {
  description = "Lifecycle rule to abort incomplete multi-part uploads after a certain time"
  default     = false
}

variable "abort_incomplete_multipart_upload_days" {
  description = "No. of days to wait before aborting incomplete multi-part uploads"
  default     = "7"
}

variable "noncurrent_version_transition_days" {
  type        = number
  default     = 30
  description = "Number of days to persist in the standard storage tier before moving to the glacier tier infrequent access tier"
}

variable "enabled" {
  type        = bool
  default     = true
  description = "Set to false to prevent the module from creating any resources"
}

variable "allow_encrypted_uploads_only" {
  type        = bool
  default     = false
  description = "Set to `true` to prevent uploads of unencrypted objects to S3 bucket"
}


variable "app_env" {
  description = "Stage (e.g. `prod`, `dev`, `staging`)"
  default     = "dev"
}




variable "versioning" {
  description = "Map containing versioning configuration." #S3 Versioning to keep multiple versions of an object in one bucket
}

/*
variable "bucket_log" {
  description = "(Optional, Forces new resource) The name of the bucket in lower case.If omitted, Terraform will assign a random, unique name."
  type        = string
}
*/
variable "acl_log" {
  description = "(Optional) The canned ACL to apply. Defaults to 'log-delivery-write'."
  type        = string
  default     = "log-delivery-write"
}

variable "attach_elb_log_delivery_policy" {
  description = "Controls if S3 bucket should have ELB log delivery policy attached"
  type        = bool
  default     = false
}

variable "attach_policy" {
  description = "Controls if S3 bucket should have bucket policy attached (set to `true` to use value of `policy` as bucket policy)"
  type        = bool
}

variable "logging" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = false
}

variable "policy" {
  description = "(Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide."
  type        = string
  default     = null
}

variable "s3_policy_rendered" {
  description = "(Required) to be passed from child account."
  type        = string
  default     = null
}
variable "s3_depends_on" {
  type    = any
  default = null
}
variable "app_name" {
  description = "Stage (e.g. `prod`, `dev`, `staging`)"
  default     = "iso"
}

variable "public_access" {
  description = "S3 public access block settings"
  type        = map(any)
  default     = {}
}

#PUT Bucket acl and PUT Object acl calls will fail if the specified ACL allows public access.
variable "block_public_acls" {
  description = "enable server access log of s3 bucket"
  type        = bool
}
#Ignore public ACLs on this bucket and any objects that it contains.
variable "ignore_public_acls" {
  description = "enable server access log of s3 bucket"
  type        = bool
}

#Reject calls to PUT Bucket policy if the specified bucket policy allows public access.
variable "block_public_policy" {
  description = "enable server access log of s3 bucket"
  type        = bool

}

#Only the bucket owner and AWS Services can access this buckets if it has a public policy.
variable "restrict_public_buckets" {
  description = "enable server access log of s3 bucket"
  type        = bool
}


variable "tags" {
  type = map(string)
}