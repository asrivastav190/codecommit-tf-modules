# AWS S3 for VPC FlowLog


#### Description 

Amazon Simple Storage Service (Amazon S3) is an object storage service that offers industry-leading scalability, data availability, security, and performance. Amazon S3 provides management features so that you can optimize, organize, and configure access to your data to meet your specific business, organizational, and compliance requirements.


## Terraform Module
```
+---S3
|       templates
        |  assume-role.json
        |  replication.json
|       Readme.md
|       locals.tf
|       s3.tf
|       outputs.tf
|       tfvars.tf
|       iam.tf
|       replica_s3.tf
```

Readme.md:  Readme file contains all the information of modules.

locals.tf: Locals are named values that you can refer to in your configuration. In this case we are passing tag values for each resource in the network firewall module.

s3.tf: This Terraform file is used to create a S3 bucket and logging bucket for the respective s3 bucket.


![Image](../images/s3/s3_tf.PNG)

outputs.tf: This is responsible for logging the S3 ID and ARN.


![Image](../images/s3/output.PNG)

iam.tf:  This is responsible for creating iam role and policy attachment resource for replication(CRR).
  
![Image](../images/s3/iam_tf.PNG)

replica_s3.tf: This is responsible for creating a replication bucket.

![Image](../images/s3/replica_s3.PNG)

assume_role.json: This is responsible for creating assume role policy.

![Image](../images/s3/assume_role.PNG)

Replication.json: This is responsible for creating a replication policy for s3 bucket.

![Image](../images/s3/replication.PNG)


tfvars.tf: This Terraform file is used to pass input variables to enable features like attached policy, SSE algorithm, versioning and block public access, which in turn is provided by root the folder variables.tf file.

## Root Structure
```
|   main.tf
|   variables.tf
|   data.tf
|  Templates
     |bucket-policy.json
```


main.tf: This file is responsible for passing variables required to call the S3 module and create its resources as defined above in a specified AWS account.

```hcl

module "create_s3" {
  source = "../modules/s3"
  aws_region                         = var.aws_region
  acl                                = var.acl
  sse_algorithm                      = var.sse_algorithm
  force_destroy                      = var.force_destroy # make this false later
  attach_policy                      = var.attach_policy # make this true later to attach bucket policy
  s3_policy_rendered                 = data.template_file.s3_bucket_policy.rendered
  noncurrent_version_transition_days = 600
  block_public_acls                  = var.block_public_acls
  block_public_policy                = var.block_public_policy
  ignore_public_acls                 = var.ignore_public_acls
  restrict_public_buckets            = var.restrict_public_buckets
  tags                               = var.tags
  versioning                         = var.versioning
}

```

Data.tf: This file is required to pass a template file for the S3 bucket.

Templates: Templates is a directory where we can store bucket policy and modify the bucket policy as per the requirements.

Bucket-policy.json: The policy attached to an S3 bucket.


![Image](../images/s3/bucket-policy.PNG)

variables.tf: Variables passed specifically for S3 modules are app name, app env used as part of tags, attach policy, SSE algorithm, force destroy, ACL and block public access.

Note: You can change variable values as per the requirements.

(To attach a required policy to a bucket we need to change account ID in the variable.)

Below is a reference screenshot:

![Image](../images/s3/variable.PNG)


##### Reference Links
https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html

##### Diagram
N/A

##### Limitations
N/A

