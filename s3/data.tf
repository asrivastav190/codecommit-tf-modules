data "template_file" "assume_role_policy" {
  template = file("${path.module}/templates/assume-role.json")
}


data "template_file" "replication_policy" {
  template = file("${path.module}/templates/replication.json")
  vars = {
    this_bucket_arn        = "${aws_s3_bucket.this.arn}"
    destination_bucket_arn = "${aws_s3_bucket.destination.arn}"
  }
}